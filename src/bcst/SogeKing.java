package bcst;
import robocode.*;
import java.awt.*;
import static robocode.util.Utils.normalRelativeAngleDegrees;

// API help : https://robocode.sourceforge.io/docs/robocode/robocode/Robot.html

/**
 * SogeKing - um robô feito por Bruno Caldas dos Santos Teodoro 
 */
public class SogeKing extends AdvancedRobot {
	int dist = 50; // distance to move when we're hit
	int turnDirection = 1; // definir sentido horário ou anti-horário

	public void run() {
		
		//definir cores do robô
		setColors();	
		
		//mover cada parte do robô livremente
		setAdjustRadarForRobotTurn(true);
		setAdjustGunForRobotTurn(true);
		setAdjustRadarForGunTurn(true);	

		while(true) {
			
			setTurnGunRight(360);  // scannear até encontrar seu primeiro inimigo
			
			//caso não ache o inimigo, scannear novamente
			if (getRadarTurnRemaining() == 0.0) {
				setTurnRadarRight(360);
			}

			execute(); // executar todas as ações set.
		}
	}
	
	public void onScannedRobot(ScannedRobotEvent e) {		
				
		// Calcular a posição exata do robô
		double absoluteBearing = getHeading() + e.getBearing();
		
		//definine para qual direção o robô irá virar
		if (e.getBearing() >= 0) {
			turnDirection = 1;
		} else {
			turnDirection = -1;
		}
		

		setTurnRight(e.getBearing()); // se move em direção ao inimigo
		
		setAhead(absoluteBearing * turnDirection); //move o robô em direção ao inimigo
		
		//armazena o ângulo normalizado do inimigo 
		double bearingFromGun = normalRelativeAngleDegrees(absoluteBearing - getGunHeading());		
		double bearingFromRadar = normalRelativeAngleDegrees(absoluteBearing - getRadarHeading());
		
		
		setTurnGunRight(bearingFromGun); //vira arma para o inimigo
		setTurnRadarRight(bearingFromRadar); // vira o radar para o inimigo
		smartFire(absoluteBearing); // controla a potencia do tiro dependendo da distância
		
		// se o radar não estiver girando, gera evento de giro (scanner)
		if (bearingFromGun == 0) {
			scan();
		}
	}
	
	//caso o robô seja acetado por um tiro ele anda em meia lua
	public void onHitByBullet(HitByBulletEvent e) {
		setTurnRight(normalRelativeAngleDegrees(e.getBearing() + 90));

		ahead(dist);
		dist *= -1;
		scan();
	}
	
	/**
	 * onHitWall: What to do when you hit a wall
	 */
	public void onHitWall(HitWallEvent e) {
		setBack(50);	
	}
	
	public void onHitRobot(HitRobotEvent e){		
		setTurnRight(normalRelativeAngleDegrees(e.getBearing() + 90));
		ahead(dist);
		dist *= -1;			
	}
	
	private void setColors() {
		setBodyColor(Color.YELLOW);
		setGunColor(Color.RED);
		setRadarColor(Color.YELLOW);
		setBulletColor(Color.GREEN);
		setScanColor(Color.RED);
	}
	
	private void smartFire(double robotDistance) {
		if (robotDistance > 200 || getEnergy() < 15) {
			fire(1);
		} else if (robotDistance > 50) {
			fire(2);
		} else {
			fire(3);
		}
	}
		
}
